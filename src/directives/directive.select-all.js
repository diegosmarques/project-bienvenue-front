export const selectAll= {  // When we create a directive to use the same on the template this directive has to be called by v-select-all 
    bind(el){
      let input =  el.querySelector('.q-field__native') // since I'm using quasar, we used a class of the real input to select the right element.
      input.addEventListener('focus', () => {
          if(input.value.length){
              input.select()
        }
      })
    }
}