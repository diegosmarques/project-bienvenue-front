import {Dialog, Loading} from 'quasar'
export function showMessage(title,Message){
    Loading.hide()
    Dialog.create({
        title : title,
        message : Message
    })

}

