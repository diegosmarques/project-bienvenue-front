import Vue from 'vue'
import Vuex from 'vuex'

 import tasks from './store-tasks'
 import settings from './store-settings'
 import auth from './store-auth'
 import documents from './store-documents'
 import visa from './store-visa'
 import demarches from './store-demarches'
 import generalData from './store-general-data'
 import translator from './store-translator'
 
 
Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */


  const Store = new Vuex.Store({
    modules: {
      tasks,
      settings,
      auth,
      documents,
      visa,
      demarches,
      generalData, 
      translator

    },
    
    

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  export default Store


