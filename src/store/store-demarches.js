import { Notify } from 'quasar'
import {showErroMessage} from 'src/functions/functions-show-error-message'
import { api } from 'boot/axios'
import Vue from 'vue'

const state = {
    checklistVisa:{},

}

const mutations = {
    UPDATE_CHECKLISTVISA(state, payload){
        Object.assign({},state.checklistVisa, payload)
    },
    DELETE_CHECKLISTVISA(state, key){
        Vue.delete(state.checklistVisa, key)
    },
    ADD_CHECKLIST(state,payload){
        Vue.set(state.checklistVisa, payload.checkListVisa)

    },
    CLEAR_DATA(state){ 
        state.checklistVisa = {}
    },
    SET_CHECKLISTVISA(state, payload){
        state.checklistVisa = payload
        
    },
    SET_DOCUMENTSDOWNLOADED(state, value){
        state.checklistVisa = value
    }
}

const actions = {
    apiReadCheckLists({ commit }) {
        api.get('/user/document/list/getChecklist')
        .then(response=>{
            if(response.status == 200 || response.status == 204){
                commit("SET_CHECKLISTVISA", response.data)
            }
        })
        .catch(error => {
            showErroMessage(error.message);
        })
    },
    apiAddCheckList({ dispatch}){
        api.post('/user/document/list/visa')
        .then(response=>{
            if(response.status == 201){
                dispatch('apiReadCheckLists')
                Notify.create('Demarche ajouté') 
            }
        })
        .catch(error => {
            showErroMessage(error.message);
        })    
    },
    apiUpdateCheckList({commit, dispatch}, payload){
        api.put('user/document/list/updateChecklist/'+payload.id, payload)
        .then(response=>{
            if(response.status == 200 || response.status == 201){
                commit('UPDATE_CHECKLISTVISA', response.data)
            }
        })
        .catch(error => {
            showErroMessage(error.message);
        })   
    }

}
const getters = {
    checkListVisa:(state)=>{
        return state.checklistVisa
    }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}