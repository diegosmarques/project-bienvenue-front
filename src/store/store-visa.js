
const state = {
    visa_cod:['VLS-TS', 'VLS'],
    visa_type:['Marié(e)','Étudiant(e)', 'Salarié(e)']

}

const mutations = {
  
}

const actions = {
    

}

const getters = {
    visa_cod: state =>{
        return state.visa_cod
    },
    visa_type: state =>{
        return state.visa_type
    },
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}