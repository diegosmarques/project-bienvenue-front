
import { LocalStorage, Loading } from 'quasar'
import {showErroMessage} from 'src/functions/functions-show-error-message'
import {showMessage} from 'src/functions/funtions-show-notification-message'
import { api } from 'boot/axios'
 
const state = {
    loggedIn:false,
    userData:{},
    formCompleted:false
}

const mutations = {
    SET_LOGGED_IN(state, value){
        
        state.loggedIn = value
    },
    SET_USER_DATA(state, payload){
        state.userData = payload
        if(state.userData.nationality == "" || state.userData.nationality == null){
            state.formCompleted = false
        }else{
            state.formCompleted = true
        
        }
        
    },
    CLEAR_DATA(state){ // This is to clear the object  when the user logout
        state.userData = {}
    },
    SET_FORM_COMPLETED(state, value){
        
        state.formCompleted = value
    },
}

const actions = {
    registerUser({}, payload){
        Loading.show()
        api.post('user/register',payload)
        .then(response =>{
            showMessage('Welcome', 'Your account was created, enjoy!')
            this.$router.push('/auth').catch(err=> {})
    
        })
        .catch(error => {
            showErroMessage(error.response.data.message);
        })
    },
    updateUser({commit}, payload){
        Loading.show()
        api.put('user/update',payload)
        .then(response =>{
            commit('SET_FORM_COMPLETED', payload)
        })
        .catch(error => {
            showErroMessage(error.response.data.message);
        }).finally(()=>{
            Loading.hide()
        })
    },

    loginUser({commit}, payload){
        Loading.show()
        api.post('login_check',{
                email: payload.email, 
                password:payload.password
        
            })
        .then(response =>{
            if(response.status == 200 || response.status == 204){
                LocalStorage.set('loggedIn', true)
                commit('SET_LOGGED_IN', true)
                this.$router.push('/').catch(err=> {})
            }
        })
        .catch(error => {
                showErroMessage("Identifiants non valides");
                
        }).finally(() => {
            Loading.hide()
        
        })
    },
    logoutUser({commit}){
        commit('SET_LOGGED_IN', false)
        commit('CLEAR_DATA')
        commit('demarches/CLEAR_DATA', null, { root: true})
        commit('documents/CLEAR_DATA', null, { root: true})
        LocalStorage.set('loggedIn', false)
        this.$router.push('/auth').catch(err=> {})
    },
    getUserData({commit}){
        Loading.show()
        api.post('/user/data',)
        .then(response =>{
            if(response.status == 201 || response.status == 204){
                commit('SET_USER_DATA', response.data)
            }         
        })
        .catch(error => {
            showErroMessage(error.response.data.message);
        }).finally(()=>{
            Loading.hide()
        })
    },
    handleAuthStateChange({commit}, value){
        Loading.hide()
        if (value == true) {
            commit('SET_LOGGED_IN', true)
            LocalStorage.set('loggedIn', true)
            this.$router.push('/').catch(err=> {})
            }
        else{
            commit('SET_LOGGED_IN', false)
            commit('demarches/CLEAR_DATA', null, { root: true})
            commit('documents/CLEAR_DATA', null, { root: true})
            commit('translator/CLEAR_DATA', null, { root: true})
            LocalStorage.set('loggedIn', false)
            this.$router.replace('/auth').catch(err=> {}) // use replace so the user can't go back with and it is necessary to login again
        }
    }
}


const getters = {
    getterUserData :(state)=>{
        return state.userData
    }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}