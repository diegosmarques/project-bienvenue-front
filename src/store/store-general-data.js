
const state = {
    nationality:['Brésilien', 'Américan', 'Canadien'],
    marital_status:['Célibataire','Marié(e)', 'Divorcé(e)', 'Veuf(ve)'],

}

const mutations = {
}

const actions = {
    

}

const getters = {
    nationality: state =>{
        return state.nationality
    },
    marital_status: state =>{
        return state.marital_status
    },
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}