import { Notify } from 'quasar'
import {showErroMessage} from 'src/functions/functions-show-error-message'
import { api } from 'boot/axios'
import Vue from 'vue'

const state = {
    documents:{},
    search:'',
    sort:'dueDate',
    documentsDownloaded:false
}

const mutations = {
    UPDATE_DOCUMENT(state, payload){
        Object.assign({},state.documents, payload)
    },
    DELETE_DOCUMENT(state, key){
        Vue.delete(state.documents, key)
    },
    ADD_DOCUMENT(state,payload){
        Vue.set(state.documents, payload.document)

    },
    CLEAR_DATA(state){ // This is to clear the object documents when the user logout
        state.documents = {}
    },
    SET_DOCUMENTS(state, payload){
        state.documents = payload
        if(state.documents.id == "" ){
            state.documentsDownloaded = false
        }else{
            state.documentsDownloaded = true
        
        }
    }
}

const actions = {
    apiReadDocuments({ commit }) {
        api.get('documents')
        .then(response=>{
            if(response.status == 200 || response.status == 204){
                commit("SET_DOCUMENTS", response.data)
            }
        })
        .catch(error => {
            
        })
    },
    apiAddDocument({commit, dispatch}, payload){
        api.post('document/save', payload)
        .then(response=>{
            if(response.status == 201){
                dispatch('apiReadDocuments')
                Notify.create('Document ajouté') 
                payload = {
                    id : response.data.id,
                    document: response.data 
                }
                
                commit('ADD_DOCUMENT', payload)
            }
        })
        .catch(error => {
            showErroMessage(error.message);
        })    
    },
    apiDeleteDocument({commit, dispatch}, id){
        api.delete('/document/delete/'+id)
        .then(response=>{
            if(response.status == 200 || response.status == 204){
                dispatch('apiReadDocuments')
                commit('DELETE_DOCUMENT', id)    
                Notify.create('Document supprimé')
            }
        })
        .catch(error => {
            showErroMessage(error.message);
        })    
    },
    apiEditDocument({commit, dispatch}, payload){
        console.log(payload)
        api.put('/document/edit/'+payload.id, payload)
        .then(response=>{
            if(response.status == 200 || response.status == 201){
                dispatch('apiReadDocuments')
                commit('UPDATE_DOCUMENT', response.data)
                Notify.create('Document mis à jour')  
            }
        })
        .catch(error => {
            showErroMessage(error.message);
            
        })   
    }

}
const getters = {
    documents:(state)=>{
        return state.documents
    },
    documentsDownloaded:(state)=>{
        return state.documentsDownloaded
    }

}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}