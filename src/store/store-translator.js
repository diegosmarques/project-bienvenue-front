
import { api } from 'boot/axios'
const state = {
    translators:{},

}

const mutations = {
    SET_TRANSLATORS(state, payload){
        state.translators = payload
    },
    CLEAR_DATA(state){ // This is to clear the object documents when the user logout
        state.translators = {}
    }
}

const actions = {
    apiGetTranslators({ commit }) {
        api.get('translator')
        .then(response=>{
            if(response.status == 200 || response.status == 204){
                commit("SET_TRANSLATORS", response.data)
            }
        })
        .catch(error => {
            
        })
    }

}

const getters = {
    getterTranslators: state =>{
        return state.translators
    },

}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}