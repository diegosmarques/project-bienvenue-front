// src/boot/axios.js
import axios from 'axios'
import Store from '../store/index.js'
import {Loading } from 'quasar'

const api = axios.create({ 
    baseURL: 'http://localhost:8000/api/',
    withCredentials :  true

})
let isRefreshing = false;
let subscribers = [];


api.interceptors.response.use(
  response => {
    return response;
  },
  err => {
    const {
      config,
      response: { status, data }
    } = err;

    const originalRequest = config;
    
    if(data.message === "Missing token") {
      Loading.show()
      Store.dispatch('auth/handleAuthStateChange', false, { root: true})
      
      return Promise.reject(false);
    }

    if (originalRequest.url.includes("api/login_check")) {
      return Promise.reject(err);
    }

    if (status === 401 && data.message === "JWT Token not found") {
      if (!isRefreshing) {
        isRefreshing = true;
        Loading.show()
          api.post('token/refresh')
          .then(({ status }) => {
            if (status === 200 || status == 204) {
              isRefreshing = false;
              Loading.hide()
            }
            subscribers = [];
          })
          .catch(error => {
            
          });
      }

      const requestSubscribers = new Promise(resolve => {
        subscribeTokenRefresh(() => {
          resolve(api(originalRequest));
        });
      });

      onRefreshed();

      return requestSubscribers;
    }
  }
);

function subscribeTokenRefresh(cb) {
  subscribers.push(cb);
}

function onRefreshed() {
  subscribers.map(cb => cb());
}

subscribers = [];



export { api }
