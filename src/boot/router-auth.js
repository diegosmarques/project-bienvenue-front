import { LocalStorage } from 'quasar'
export default  ({  router }) => {
  router.beforeEach((to, from, next) => {
    let loggedIn = LocalStorage.getItem('loggedIn') 
    if(!loggedIn && to.path !== '/auth'){  // nao esquecer de colocar a negacao para obrigar o usuario logar
        next('/auth')
    }
    else {
      next()
    }
    
  })
}
