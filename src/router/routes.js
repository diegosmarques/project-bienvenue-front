
const routes = [
  {
    path: '/',
    component: () => import('src/layouts/Layout.vue'),
    children: [
      { path: '', component: () => import('pages/PageDemarches.vue') },
      { path: '/settings', component: () => import('pages/PageSettings.vue') },
      { path: '/documents', component: () => import('pages/PageDocuments.vue') },
      { path: '/appointement', component: () => import('pages/PageAppointement.vue') },
      { path: '/services', component: () => import('pages/PageService.vue') },
      { path: '/auth', component: () => import('pages/PageAuth.vue') },
      

    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
  
]

export default routes
